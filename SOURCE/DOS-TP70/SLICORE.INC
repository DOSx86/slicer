{

BSD 2-Clause License

Copyright (c) 2019-2022, Jerome Shidel
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

}

uses QCrt, QAsciiZ, QDOS, QStrings, QClass, QLists, QFiles;

const
	{$IFDEF Express}
    Title           : String[20] = 'File Slicer Express';
    {$ELSE}
    Title           : String[20] = 'File Slicer';
    {$ENDIF}
    ExeBaseName     : string[8]  = 'SLICER';
    Version         : string[16] = '0.24 (BETA)';
    ThisYear		: String[4]  = '2022';
    CRLF            : String[2]  = #13#10;
    ArchiveID		: String[24] =  'File Slicer Archive ';
    Format          : String[16] = 'v1.0'; { Archive Format Version }
    SupVersion      : word       = $0100;  { Archive file version supported }
    Extension       : String[3]  = 'SAF';
    EOF_char        : Char       = #26;
    NULL_char       { : Char }   = #0;
    CR_char         { : Char }   = #13;
    LF_char         { : Char }   = #10;
    SPACE_char      { : Char }   = #32;
    BufferSize      : word       = 4 * 1024;
    DefaultCategory : string[20] = 'default';
    DirCache        : LongInt    = 1000;
    MaxNotesSize    : Longint    = 16 * 1024;
    FormatDelim     { : Char }   = '|'; { Internal String formating separator }
    StringDelim     { : Char }   = ';'; { Internal String list separator }
    CategoryDelim   { : Char }   = ','; { category list separator }
    CreateMode      { : word }  = faArchive; { Default file attribs for OS }

{$IFDEF ExtractLog}
	ExLogName : String = ''; { Assigned later }
var
	ExLog : text;
{$ENDIF}


type
    PCharArray = ^TCharArray;
    TCharArray = array [0..65534] of Char;
    PByteArray = ^TCharArray;
    TByteArray = array [0..65534] of Byte;
    TStamper = record
        Date : String;
        Time : String;
        AM : String;
        PM : String;
        Stamp : String;
    end;

    TFloppySize = record
        Name  : String[10];
        Class : byte;
        Bytes : LongInt;
    end;

const
    Floppies : array[0..11] of TFloppySize = (  { Some of these need verified }
        (Name:'1.44'; Class:0; Bytes:1474560 ), { default slice size }
        (Name:'160';  Class:0; Bytes:160*1024 ),
        (Name:'180';  Class:0; Bytes:180*1024 ),
        (Name:'320';  Class:0; Bytes:320*1024 ),
        (Name:'360';  Class:0; Bytes:360*1024 ),
        (Name:'720';  Class:0; Bytes:737280 ),
        (Name:'1.2';  Class:0; Bytes:1228*1024 ),
        (Name:'2.88'; Class:0; Bytes:1474560 * 2 ),
        { Alternate names for some floppy sizes }
        (Name:'1.4';  Class:0; Bytes:1474560 ),
        (Name:'2.8';  Class:0; Bytes:1474560 * 2 ),
        (Name:'144';  Class:0; Bytes:1474560 ),
        (Name:'288';  Class:0; Bytes:1474560 * 2 )
    );

    { App Error Codes }
    erFile_Not_Found     = 2;       { file not found }
    erPath_Not_Found     = 3;       { path not found }
    erAccess_Denied      = 5;       { file access denied }
    erOut_of_Memory      = 8;       { insufficient memory }
    erInvalid_Format     = 11;      { invalid disk format }
    erInvalid_Data       = 13;      { Invalid data }
    erUser_Terminated    = 14;      { Reserved }
    erData_Error         = 23;      { CRC Error }
    erInternal_Error     = 81;      { Reserved }
    erCommand_Line_Error = 87;      { Invalid Paramater }
    erNo_Command_Line    = 87;

    cpNone				 = 0;		{ no compression }
    cpGZip				 = $101; 	{ pass through gzip }

    { Always immediately fatal errors }
    FatalErrors : String =
            { Chr(erUser_Terminated) + }
            Chr(erOut_of_Memory) +
            Chr(erCommand_Line_Error);

    { Text Display Message Classes }
    mcAlways   = 0;
    mcCritical = 1;
    mcGeneral  = 2;
    mcVerbose  = 3;

    { Application Primary mode of execution }
    pmUnknown    = 0;
    pmExtract    = 1;
    pmCreate     = 2;
    pmAppend     = 3;
    pmUpdate     = 4;
    pmSlice      = 5;
    pmReport     = 6;

    spSwitches   : String[12] = 'fieIEsLgmOMp'; { Switches that require a param }
    pmSwitches   : String[6] = 'xcruSR';        { Execution mode switches }

{ Global Variables }
var
    Messages     : pointer;   { Pointer to Language Specific Messages }
    MessagesSize : word;      { Size of Language Specific Messages }

    ExecMode   : byte;      { Primary execution mode }
    Output     : String;    { base extraction path }
    Testing    : boolean;   { test mode }
    NoSubDir   : boolean;   { Do not scan / include subdirectories }
    NoEmptyDir : boolean;   { Do not include empty subdirectories }
    Verbose    : integer;   { Comment level, -2 silent, -1 quite, 0 normal, 1 verbose }
    Overwrite  : integer;   { Overwrite existing files. -1 no, 0 ask, 1 yes }
    ErrorDie   : integer;   { Die on (some) errors. -1 no, 0 ask, 1 yes }
    AcceptAll  : integer;   { General User Prompts. -1 no, 0 ask, 1 yes }
    CheckCase  : integer;   { ignore file case, -1 no, 0 default, 1 yes }
    Slicing    : longint;   { Maximum size of sliced archive files }
    Attribs    : word;      { File Search Attributes }
    Compression: word;		{ Process files through compression >$100 is external }
    CompressApp: String;	{ External compression program being used }
    Archive    : String;    { Archive file name }
    Category   : String;    { Category for adding/extracting items }
    Language   : String;    { Language used for embedded archive text }
    Embedded   : String;    { File to embed as message }
    EmbedConf  : boolean;   { Embedded file requires user confirmation }
    EmbedLang  : String;    { Embedded text language, default * }
    WantBanner : boolean;   { State of program banner shown }
    WatchMedia : boolean;   { Watch for media change with missing file prompt }
    WaitMedia  : integer;   { Timer ticks to wait after change detected }
    TimerTick  : LongInt absolute $0040:$006c;  { Timer tick counter }
    MemSize,
    MemUsed    : longint;
    StackSize,
    StackMin   : Word;
    Bytes,
    KBytes,
    MBytes     : String[20]; { Size Suffix Strings }

    {$IFOPT D+}
    DebugMode  : boolean;
    {$ENDIF}

    IncludeList, ExcludeList : PStringList;

    {$IFDEF FileTemp}
    	FileTemp : PDiskFile;
    {$ENDIF}
    Stamper : TStamper;

const
    clNormal        = 256;
    {$IFOPT D+}
        clDebug         = clNormal + 1;
        clDebugClass    = clDebug + 1;
        clDebugText     = clDebugClass + 1;
        clDebugPass     = clDebugText + 1;
        clDebugFail     = clDebugPass + 1;
        clDebugAppend   = clDebugFail + 1;
        clDebugMore     = clDebugAppend + 1;
        clBanner        = clDebugMore + 1;
    {$ELSE}
        clBanner        = clNormal + 1;
    {$ENDIF}
    clFatalLabel    = clBanner + 1;
    clFatalText     = clFatalLabel + 1;
    clWarningLabel  = clFatalText + 1;
    clWarningText   = clWarningLabel + 1;
    clExcludeFile   = clWarningText + 1;
    clHeadID        = clExcludeFile + 1;
    clHeadDate      = clHeadID + 1;
    clHeadNote      = clHeadDate + 1;
    clEmbedText     = clHeadNote + 1;
    clAcceptText    = clEmbedText + 1;
    clAcceptPrompt  = clAcceptText + 1;
    clAcceptYes     = clAcceptPrompt + 1;
    clAcceptNo      = clAcceptYes + 1;
    clSkipped       = clAcceptNo + 1;
    clOverwrite     = clSkipped + 1;
    clMedia         = clOverwrite + 1;
    clPause         = clMedia + 1;

    clMaximum       = clPause;

    ThemeColors : array[clNormal..clMaximum] of byte = (
        LightGray,          { clNormal }
        {$IFOPT D+}
            DarkGray,
            Cyan,
            LightCyan,
            LightGreen,
            LightRed,
            Yellow,
            White,
        {$ENDIF}
        White,              { clBanner }
        LightRed,           { clFatalLabel }
        LightGray,          { clFatalText }
        Yellow,             { clWarningLabel }
        LightGray,          { clWarningText }
        LightRed,           { clExcludeFile }
        DarkGray,           { clHeadID }
        DarkGray,           { clHeadDate }
        Cyan,               { clHeadNote }
        White,              { clEmbedText }
        White,              { clAcceptText }
        LightGray,          { clAcceptPrompt }
        LightGreen,         { clAcceptYes }
        LightRed,           { clAcceptNo }
        Red,                { clSkipped }
        Green,              { clOverwrite }
        LightGray,          { clMedia }
        DarkGray            { clPause }
    );

function StackAvail : word; assembler;
asm
	push sp
	pop  ax
end;

{$IFOPT D+}
procedure Debug(S1, S2 : String); forward;
{$ENDIF}

function NoDoubleSlash(S : String) : String; forward;
procedure UserAccept (var Prompt : boolean); forward;
function AskUser (Msg : String; Data : String) : boolean; forward;
function CheckAbort : boolean; forward;
procedure CleanUp; forward;
procedure CheckError(F : PAbstractFile; Fatal : boolean); forward;
function GetIOResult(S : String; Fatal : Boolean) : integer; forward;
procedure ChangeMedia(F : PAbstractFile); forward;
procedure MakeError(F : PAbstractFile; Error : integer; Fatal : boolean); forward;
procedure CheckMemory(Required:word); forward;
procedure ThemeColor(Element:integer); forward;
procedure ShowError(ID, Data : String; ErrorCode : Byte; Terminate : boolean ); forward;

{$IFDEF Express}
	procedure BuiltInEnglish; assembler;
	asm
		db 'LANGUAGE=en',0
	    db 'TITLE=%, Version %',0
	    db 'COPYRIGHT=Copyright (c) 2019-%, Jerome Shidel',0
		db 0,0
	end;
{$ELSE}
	{$I ENGLISH.INC}
{$ENDIF}

{$I FILETOOL.INC}
{$I STRCACHE.INC}
{$I MESSAGES.INC}
{$I MATCHING.INC}
{$I PARAMS.INC}
{$I ARCHIVE.INC}


var
    FileIn, FileOut : PArchiveFile;

function NoDoubleSlash(S : String) : String;
var
    P : integer;
begin
    repeat
        P := Pos('\\', S);
        if P > 0 then System.Delete(S, P, 1);
    until P <= 0;
    NoDoubleSlash := S;
end;

procedure ThemeColor(Element:integer);
begin
    if (Element < clNormal) or (Element > clMaximum) then
        ShowError('', IntStr(erInternal_Error), erInternal_Error, True);
    TextColor(ThemeColors[Element]);
end;

{$IFOPT D+}
const
    DebugIndent : integer = 0;
    DebugDelay : word = 0;

{$IFDEF ExtractLog}
procedure Debug(S1, S2 : String);
var
    X : integer;
begin
	 if ExLogName = '' then exit;
    if not DebugMode then exit;
    if S1[1] = '-' then begin
        if DebugIndent > 1 then Dec(DebugIndent, 2);
        if (S1 = '-') and (S2 = '') then exit;
    end;
    FileMode := 2;
	System.Append(ExLog);
    if DebugIndent > 0 then
        System.Write(ExLog, Space(DebugIndent));
    if S1[1] <> '/' then begin
        System.Write(ExLog, 'debug ');
        if S1 <> '' then
            System.Write(ExLog, S1 + WhichStr(S2 = '', ': ', ''))
        else
            System.Write(ExLog, S1 + '--> ');
    end else begin
       System.Write(ExLog, 'result: ')
    end;
    if Pos(SPACE_char, S2) < 1 then
        System.Write(ExLog, S2)
    else begin
        System.Write(ExLog, Copy(S2, 1, Pos(SPACE_char, S2)));
        System.Write(ExLog, Copy(S2, Pos(SPACE_char, S2)+ 1, Length(S2)));
    end;
    System.WriteLn(ExLog, '');
    if (MaxAvail < 10240) or (StackAvail < 4096) then
	    System.WriteLn(ExLog, 'LOW - MEM: ', MemAvail, ' MAX: ', MaxAvail, ' STACK: ', StackAvail);
    if S1[1] = '+' then
        Inc(DebugIndent, 2);
	System.Close(ExLog);
end;
{$ELSE}
procedure Debug(S1, S2 : String);
var
    X : integer;
begin
    if not DebugMode then exit;
    X := TextAttr;
    if S1[1] = '-' then begin
        if DebugIndent > 1 then Dec(DebugIndent, 2);
        if (S1 = '-') and (S2 = '') then exit;
    end;
    if DebugIndent > 0 then
        ShowText(mcAlways, Space(DebugIndent));
    if S1[1] <> '/' then begin
        ThemeColor(clDebug);
        ShowText(mcAlways, 'debug ');
        ThemeColor(clDebugClass);
        if S1 <> '' then
            ShowText(mcAlways, S1 + WhichStr(S2 = '', ': ', ''))
        else
            ShowText(mcAlways, S1 + '--> ');
        ThemeColor(clDebugText);
    end else begin
        if IsRedirectedOutput then begin
            ShowText(mcAlways, 'result: ')
        end else begin
            GotoXY ( 1, WhereY - 1 );
            EOL;
            GotoXY ( WhereX + 1, WhereY );
            ShowText(mcAlways, SPACE_char);
            if (Copy(S2, 1, Pos(SPACE_char, S2)) = '0 ') or (S2 = '0') then
                ThemeColor(clDebugPass)
            else
                ThemeColor(clDebugFail);
        end;
    end;
    if Pos(SPACE_char, S2) < 1 then
        ShowText(mcAlways, S2)
    else begin
        ShowText(mcAlways, Copy(S2, 1, Pos(SPACE_char, S2)));
        if S1[1] = '/' then
            ThemeColor(clDebugAppend)
        else
            ThemeColor(clDebugMore);
        ShowText(mcAlways, Copy(S2, Pos(SPACE_char, S2)+ 1, Length(S2)));
    end;
    TextAttr := X;
    ShowTextLn(mcAlways, '');
    if S1[1] = '+' then
        Inc(DebugIndent, 2);
    if (not IsRedirectedOutput) and (DebugDelay > 0) then
        Delay(DebugDelay);
end;

{$ENDIF}

{$ENDIF}

function AskUser (Msg : String; Data : String) : boolean;
var
    PY, PN : string[1];
    PYes, PNo : String[20];
    PA, P : String;
    I, T : integer;
    DefaultYes : boolean;
begin
    AskUser := True;
    if ((Msg = 'OVER') and (Overwrite <> 0)) then begin
        AskUser := Overwrite > 0;
        Exit;
    end;
    if not IsRedirectedOutput then begin
        While Keypressed do ReadKey;
    end;
    DefaultYes := Msg <> 'OVER';
    P := 'PROMPT_';
    PY := ParseMessage(P + 'Y', '');
    PN := ParseMessage(P + 'N', '');
    PYes := ParseMessage(P + 'YES', '');
    PNo := ParseMessage(P + 'NO', '');
    I := Pos(UCase(PY), UCase(PYes));
    if I > 0 then begin
        Delete(PYes, I, 1);
        Insert('(' + PY + ')', PYes, I);
    end;
    I := Pos(UCase(PN), UCase(PNo));
    if I > 0 then begin
        Delete(PNo, I, 1);
        Insert('(' + PN + ')', PNo, I);
    end;
    PA := ParseMessage(P + Msg, WhichStr(DefaultYes,
        PNo + '/' + PYes, PYes + '/' + PNo) + FormatDelim + Data) + SPACE_char;
    if not IsRedirectedOutput then begin
        if WhereX >= Lo(WindMax) - Length(PA) - Length(PYes + '/' + PNo) then
            ShowTextLn(mcCritical, '')
        else if WhereX > 1 then
            ShowText(mcCritical, SPACE_char)
    end else
        ShowTextLn(mcCritical, '');
    ThemeColor(clAcceptPrompt);
    I := 0;
    T := 0;
    while I < Length(PA) do begin
        Inc(I);
        if (PA[I] = '(') then Inc(T);
        case T of
            0,2,4 : ThemeColor(clAcceptPrompt);
            1 : if DefaultYes then ThemeColor(clAcceptYes) else ThemeColor(clAcceptNo);
            3 : if DefaultYes then ThemeColor(clAcceptNo) else ThemeColor(clAcceptYes);
        end;
        ShowText(mcCritical, PA[I]);
        if (PA[I] = ')') then Inc(T);
    end;
    if not IsRedirectedOutput then begin
        ThemeColor(clNormal);
        ShowText(mcCritical, ' ');
        GotoXY(WhereX - 1, WhereY);
    end;

    PA := '';
    if (DefaultYes and (AcceptAll > 0)) then
        PA := UCase(PY)
    else if IsRedirectedOutput then
        PA := UCase(PN);

    if PA = '' then
        while (UCase(PA) <> UCase(PY)) and (UCase(PA) <> UCase(PN)) do begin
            While not Keypressed do Idle;
            PA := ReadKey;
            if (PA = CR_char) or (PA = SPACE_char) then
                PA :=  WhichStr(DefaultYes, PN, PY);
            if PA = #3 then begin
                ThemeColor(clNormal);
                ShowTextLn(mcCritical, '');
                ShowError('', IntStr(erUser_Terminated), erUser_Terminated, True);
            end;
        end;

    if UCase(PA) = UCase(PY) then
        ThemeColor(clAcceptYes)
    else
        ThemeColor(clAcceptNo);
    ShowText(mcCritical, WhichStr(UCase(PA) = UCase(PY),
        ParseMessage(P + 'NO', ''),
        ParseMessage(P + 'YES', '')));
    ThemeColor(clNormal);
    ShowTextLn(mcCritical, '');
    AskUser := (UCase(PA) = UCase(PY));
end;

procedure UserAccept (var Prompt : boolean);
begin
    if Prompt = false then exit;
    Prompt := False;
    if not AskUser('ACCEPT', '') then
        ShowError('', IntStr(erUser_Terminated), erUser_Terminated, True);
end;

function CheckAbort : boolean;
var
    C : Char;
begin
    CheckAbort := false;
    if Keypressed then begin
        While Keypressed do begin
            C := ReadKey;
            if C = #3 then begin
                ShowError('', IntStr(erUser_Terminated), erUser_Terminated, True);
                CheckAbort := true;
            end;
        end;
    end;
end;

procedure CheckError(F : PAbstractFile; Fatal : Boolean );
begin
    CheckAbort;
    {$IFOPT D+} Debug('checkerror', F^.Name +
        ' type ' + WhichStr(Fatal, 'warning', 'fatal') + ' #' + IntStr(F^.Result)); {$ENDIF}
    if F^.Result  <> 0 then
        ShowError('', IntStr(F^.Result) + FormatDelim + F^.Name , F^.Result, Fatal);
end;

function GetIOResult(S : String; Fatal : Boolean) : integer;
{$I-}
var
	E : integer;
begin
    CheckAbort;
    E := System.IOResult;
    {$IFOPT D+} Debug('IO check', S +
        ' type ' + WhichStr(Fatal, 'warning', 'fatal') + ' #' + IntStr(E)); {$ENDIF}
    if E <> 0 then
        ShowError('', IntStr(E) + FormatDelim + S , E, Fatal);
    GetIOResult := E;
end;

procedure ChangeMedia(F : PAbstractFile);
var
    R : String;
    D : byte;
    T, M, W : boolean;
    TT, TW : LongInt;
begin
    { Unlike ShowText, this uses System.Write that is upgraded by QCrt and will
    always display on the screen in text mode regardless of I/O redirection.
    Also, Keypressed and ReadKey use the BIOS and are not effected Byte
    I/O redirection.  }
    R := FileExpand(F^.Name);
    D := Byte(R[1]) - $40;
    ThemeColor(clNormal);
    System.WriteLn;
    ThemeColor(clMedia);
    System.Write(ParseMessage('MEDIA', NoDoubleSlash(F^.Name) + FormatDelim +
        Char(D + $40) + ':'));
    ThemeColor(clNormal);
    System.WriteLn;
    While Keypressed do ReadKey;
    ThemeColor(clPause);
    System.Write(ParseMessage('PAUSE', ''));
    ThemeColor(clNormal);
    W := True;
    M := True;
    TW := WaitMedia;
    While (not Keypressed) and (W or (TW > 0)) do begin
        if WatchMedia then begin
            if W then begin
                T := DiskReady(D);
                if T <> M then begin
                    if T then  W := False;
                    M := T;
                end;
            end else if (TT <> TimerTick) then begin
                TT := TimerTick;
                Dec(TW);
            end;
        end;
        Idle;
    end;
    if Keypressed then
        R := ReadKey
    else
        R := '';
    System.WriteLn;
    if R = #3 then
        MakeError(F, erFile_Not_Found, True);
end;

procedure MakeError(F : PAbstractFile; Error : integer; Fatal : boolean);
begin
    F^.Result := Error;
    CheckError(F, Fatal);
end;

procedure CleanUp;
begin
    {$IFOPT D+} Debug('+cleanup', ''); {$ENDIF}
    if Assigned(FileIn) then Dispose(FileIn, Destroy);
    if Assigned(FileOut) then Dispose(FileOut, Destroy);
    {$IFDEF FileTemp}
	    if Assigned(FileTemp) then Dispose(FileTemp, Destroy);
	    FileTemp := nil;
	{$ENDIF}
    FileIn := nil;
    FileOut := nil;

{   The Memory Heap is destroyed anyway. So, just don't bother with doing these
    cleanup items.

    if Assigned(IncludeList) then Dispose(IncludeList, Destroy);
    if Assigned(ExcludeList) then Dispose(ExcludeList, Destroy);
    IncludeList := nil;
    ExcludeList := nil;
}
{$IFOPT D+} Debug('-', ''); {$ENDIF}
end;

procedure CheckMemory(Required:word);
begin
	if StackAvail < StackMin then StackMin := StackAvail;
    if MaxAvail < Required + 1024 then
        ShowError('', IntStr(erOut_of_Memory), erOut_of_Memory, true );
    if MemAvail - Required < MemSize - MemUsed then
        MemUsed := MemSize - (MemAvail - Required);
end;

{ Initialize program, global variables }
procedure Init;
var
    I, C : integer;
begin
    {$IFOPT D+}
    DebugMode := False;
    {$ENDIF}

	{$IFDEF ExtractLog}
	ExLogName := UCase(GetEnv('SLICER.LOG'));
	if ExLogName <> '' then begin
		{$IFOPT D+}
		DebugMode := True;
		{$ENDIF}
		System.Assign(ExLog, ExLogName);
		System.Rewrite(ExLog);
		System.WriteLn(ExLog, 'Started in ' + GetCurDir);
		System.WriteLn(ExLog, 'DOS Version: ',
		  IntStr(Hi(DosVersion)) + '.' + IntStr(Lo(DosVersion)),
		  ',', HexByte(DosOEM));
		System.WriteLn(ExLog, 'Command line:');
		for I := 0 to ParamCount do
			System.WriteLn(ExLog, '  "', ParamStr(I), '"');
		System.WriteLn(ExLog, 'ENV Table:');
		for I := 1 to GetEnvCount do
			System.WriteLn(ExLog, '  ', GetEnvParam(I));
		System.Close(ExLog);
	end;
	{$ENDIF}

	{$IFDEF Express}
	FILEMODE := 0;
	{$ENDIF}
	Randomize;
    MemUsed := 0;
    MemSize := MemAvail;
    StackMin := StackSize;

    FileIn  := nil;
    FileOut := nil;
    {$IFDEF FileTemp}
    FileTemp := nil;
    {$ENDIF}

    CheckMemory(Sizeof(TStringList) * 2);
    IncludeList := New(PStringList, Create(nil));
    ExcludeList := New(PStringList, Create(nil));

    MessagesSize := 0;

    LoadLanguage(GetEnv('LANG'));

    WantBanner  := True;
    ExecMode    := pmUnknown;
    Verbose     := 0;
    Testing     := False;
    CheckCase   := 0;
    NoSubDir    := False;
    NoEmptyDir  := False;
    Overwrite   := 0;
    ErrorDie    := 0;
    AcceptAll   := 0;
    Slicing     := Floppies[0].Bytes;
    Attribs     := faReadOnly or faArchive;
    Compression := $0000;
    CompressApp := '';
    Archive     := '';
    Embedded    := '';
    EmbedConf   := False;
    EmbedLang   := '*';
    Output      := '';
    Category    := DefaultCategory;
    WatchMedia  := False;
    WaitMedia   := 18 * 1;

    ParseCmdLn;
end;

procedure ExtractArchive(ReportOnly : boolean);
begin
    ShowBanner(mcGeneral);
    CheckMemory(Sizeof(TArchiveFile));
    FileIn := New(PArchiveFile, Create(nil));
{    FileIn^.Testing := Testing; }
    FileIn^.SetMode(flRandom);
    FileIn^.Assign(Archive);
    FileIn^.Categories := Category;
    if ReportOnly then
        FileIn^.ReportArchive
    else
        FileIn^.ExtractArchive;
    Cleanup;
end;

procedure AppendArchive(CreateNew : boolean);
var
	S : String;
begin
	S := GetEnv('TEMP');
	if (S = '') or (not DirExists(S)) then
		ShowError('BAD_TMP', '3' + FormatDelim + S, erPath_not_found, true );

    case Compression of
    	cpGZip : S := 'GZip';
    else
    	S := '';
    end;
	if S <> '' then System.WriteLn(ParseMessage('USE_COMP', S));

    CheckMemory(Sizeof(TArchiveFile));
    FileOut := New(PArchiveFile, Create(nil));
    FileOut^.TestOnly := Testing;
    FileOut^.SetMode(flRandom);
    FileOut^.Assign(Archive);
    if CreateNew then begin
        if Embedded <> '' then FileOut^.SetNotes(Embedded);
        FileOut^.CreateArchive;
        Embedded := '';
    end else
        FileOut^.AppendArchive;

    FileOut^.Category:= Category;

    if Embedded <> '' then FileOut^.AddText(Embedded);

    FileOut^.AppendScan;

    CleanUp;
end;

procedure Main;
begin
    ShowBanner(mcGeneral);
    case ExecMode of
        pmExtract       : ExtractArchive(False);
        pmCreate        : AppendArchive(True);
        pmAppend        : AppendArchive(False);
        pmUpdate        : begin end;
        pmSlice         : begin end;
        pmReport        : ExtractArchive(True);
    else { pmUnknown }
    	NeedHelp;
    end;
end;

procedure Done;
begin
    {$IFOPT D+} Debug('+Done', ''); {$ENDIF}
    CleanUp;
    {$IFOPT D+} Debug('Max Memory Avail', IntStr(MemSize)); {$ENDIF}
    {$IFOPT D+} Debug('Max Memory Used', IntStr(MemUsed)); {$ENDIF}
    {$IFOPT D+} Debug('Max Stack Used', IntStr(StackSize - StackMin)); {$ENDIF}
    {$IFOPT D+} Debug('-', ''); {$ENDIF}
    {$IFOPT D+} Debug('Finished', ''); {$ENDIF}
	{$IFDEF ExtractLog}
	if ExLogName <> '' then begin
		System.Append(ExLog);
		System.WriteLn(ExLog, 'Done');
		System.Close(ExLog);
	end;
	{$ENDIF}
end;

