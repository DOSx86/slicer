function ExpandFileWith(AWild, AKnown:String) : String;
    var
        I : integer;
        R : String;
begin
    R := '';
    I := 0;
    while I < Length(AWild) do begin
        Inc(I);
        if AWild[I] = '?' then begin
            if I <= Length(AKNown) then
                R := R + AKnown[I]
        end else
        if AWild[I] = '*' then begin
            R := R + Copy(AKnown, I, Length(AKnown));
            I := 255
        end else
            R := R + AWild[I]

    end;
    ExpandFileWith := R;
end;

function ExpandPathWith(AWild, AKnown : String) : String;
var
    R : String;
    TW, TK : String;
    PW, PK : integer;
begin
    TW := AWild;
    TK := AKnown;
    R := '';
    while TW <> '' do begin
        PW := Length(TW); while (PW > 1) and (TW[PW] <> PathDelim) do dec(PW);
        PK := Length(TK); while (PK > 1) and (TK[PK] <> PathDelim) do dec(PK);
        R := ExpandFileWith(Copy(TW, PW, Length(TW)), Copy(TK, PK, Length(TK))) + R;
{
        ShowTextLn(mcAlways, Copy(TW, PW, Length(TW)) + ' + ' +
            Copy(TK, PK, Length(TK)) + ' = ' +
            ExpandFileWith(Copy(TW, PW, Length(TW)), Copy(TK, PK, Length(TK))) +
            ' > ' + R );
}
        if PW > 0 then  TW[0] := Char(PW - 1);
        if PK > 0 then TK[0] := Char(PK - 1);
    end;
    if (Length(AKnown) > Length(R)) and (R[1] <> PathDelim) then
        R := Copy(AKnown, 1, Length(AKnown) - Length(R)) + R;
{
    ShowTextLn(mcVerbose, AWild + ' - ' + AKnown + ' : ' + R);
}
    ExpandPathWith := R
end;

function FileMatch(AWildcard, AFileName : String) : boolean;
var
    P : integer;
    F, FE : String;
    W, WE : String;
begin
    FileMatch := False;
    if CheckCase < 1 then begin
        AWildcard := Ucase(AWildCard);
        AFileName := Ucase(AFileName);
    end;
    F := PathOf(AFileName);
    W := ExpandPathWith(PathOf(AWildCard), F);
    if (W <> '') and (W <> F) then exit;
    F := BaseNameOf(AFileName);
    FE := ExtensionOf(AFileName);
    W := ExpandFileWith(BaseNameOf(AWildCard), F);
    WE := ExpandFileWith(ExtensionOf(AWildCard), FE);
    if (W + WE = '') then Exit;
    if (W <> F) then exit;
    if (WE <> FE) then exit;
    FileMatch := True;
end;

function IsExcluded(AFileName : String) : boolean;
var
    Item : PStringItem;
    S : String;
    X : byte;
begin
    Item := PStringItem(ExcludeList^.GetFirst);
    while Assigned(Item) do begin
        S := Item^.GetString;
        if FileMatch(Item^.GetString, AFileName) then begin
            IsExcluded := True;
            X := TextAttr;
            ThemeColor(clExcludeFile);
            ShowText(mcVerbose, ParseMessage('EXCLUDE', AFileName + FormatDelim + Item^.GetString));
            TextAttr :=  X;
            ShowTextLn(mcVerbose,'');
            Exit;
        end;
        Item := PStringItem(Item^.GetNext);
    end;
    IsExcluded := False;
end;
