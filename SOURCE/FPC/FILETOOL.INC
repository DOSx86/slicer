function FindExecutable(FileName : String) : string;
const
	Exts : array[0..2] of Str4 = ('.EXE', '.COM', '.BAT');
var
	C, P, F, E, S : String;
	I : integer;
begin
	FindExecutable := '';
	F := BaseNameOf(FileName);
	E := ExtensionOf(FileName);
	if E <> '' then F := F + '.' + E;
	P := GetEnv('PATH');
	C := GetExePath;
	repeat
		S := TailDelim(UCase(C)) + F;
		if E <> '' then begin
			if FileExists(S) then Break;
			S := '';
		end else begin
			for I := Low(Exts) to High(Exts) do begin
				S := TailDelim(UCase(C)) + F + Exts[I];
				if FileExists(S) then Break;
				S := '';
			end;
		end;
		C := PullStr(';', P);
	until (S <> '') or (C = '');
	FindExecutable := S;
end;

function RandomStr(Count : integer) : String;
var
	I : byte;
	S : String;
begin
	S := '';
	While Length(S) < Count do begin
		I := Random(36);
		if I > 9 then
			S := S + Chr(I + 55)
		else
			S := S + Chr(I + 48);
	end;
	RandomStr := S;
end;

function GetCurDir : String;
var
  S: String;
begin
	System.GetDir(0,S);
	GetCurDir := S;
end;

function mkTempFile(Dir : String; var FileName : String) : integer;
{$I-}
var
	P, Ext : String;
	F : File;
	E, X : integer;
	M : word;
begin
	P := Dir;
	if P = '' then P := GetEnv('TEMP');
	if P = '' then P := 'C:\TEMP';
	P := TailDelim(P);
	Ext := ExtensionOf(FileName);
	if Ext = '' then Ext := 'TMP';
	{ M := FileMode; }
	{ FileMode := 0; }
	repeat
		FileName := P + RandomStr(8) + '.' + Ext;
		Reset(F,1);
		E := IOResult;
		if E = 0 then begin
			Close(F);
			E := IOResult;
		end else begin
			{ FileMode := M; }
			Assign(F, FileName);
			Rewrite(F, 1);
			E := IOResult;
			Close(F);
			X := IOResult;
			if E = 0 then E := X;
			break;
		end;
	until E <> 0;
	if E <> 0 then FileName := '';
	{ FileMode := M; }
	{$IFOPT D+} Debug('TEMP FILE', DIR + ' > ' + FileName + ' = ' + IntStr(E)); {$ENDIF}
	mkTempFile := E;
end;

var
	FileCopyBuf : array[0..4095] of byte;

function CopyFile(ASrc, ADest : String) : integer;
{$I-}
var
	FI, FO : File;
	EI, EO : integer;
	Count : integer;
	FM : word;
	X : longInt;
begin
	{ FM := FileMode;
	FM := 0; }
	Assign(Fi, ASrc);
	Reset(FI, 1);
	{ FileMode := FM; }
	EI := IOResult;
	if EI <> 0 then begin
		CopyFile := EI;
		Exit;
	end;
	Assign(FO, ADest);
	Rewrite(FO, 1);
	EI := IOResult;
	X := 0;
	while EI = 0 do begin
		BlockRead(FI, FileCopyBuf, Sizeof(FileCopyBuf), Count);
		Inc(X, Count);
		EI := IOResult;
		if Count = 0 then Break;
		if EI = 0 then begin
			BlockWrite(FO, FileCopyBuf, Count);
			EI := IOResult;
		end;
	end;
	{ Write(ASrc, '->', ADest, ' ', X, ' bytes'); }
	Close(FI);
	EO := IOResult;
	if EI = 0 then EI := EO;
	Close(FO);
	EO := IOResult;
	if EI = 0 then EI := EO;
	if EI <> 0 then begin
		{ WriteLn(', error ', EI); }
		Erase(FO);
		EO := IOResult;
	end;
	{$IFOPT D+} Debug('COPY FILE', ASRC + ' >> ' + ADEST + ' = ' + IntStr(EI)); {$ENDIF}
	CopyFile := EI;
end;

function DeleteFile(AFilename : String) : integer;
{$I-}
var
	F : File;
	EI : integer;
begin
	DeleteFile := 0;
	if AFileName = '' then exit;
	Assign(F, AFileName);
	Erase(F);
	EI := IOResult;
	{$IFOPT D+} Debug('DEL FILE', AFilename + ' = ' + IntStr(EI)); {$ENDIF}
	DeleteFile := EI;
end;

function FileRename(ASrc, ADest : String) : integer;
{$I-}
var
	F : File;
	EI : integer;
begin
	Assign(F, ASrc);
	Rename(F, ADest);
	EI := IOResult;
	{$IFOPT D+} Debug('RENAME FILE', ASRC + ' >> ' + ADEST + ' = ' + IntStr(EI)); {$ENDIF}
	FileRename := EI;
end;

const
	LastDCM : word = cpNone;
	LastDCE : string = '';

procedure DecompressFile(AFileName: String; AMethod : word);
var
	TempPath, TempName, CheckName : String;
	E : integer;
	procedure DecompressFail;
	begin
		DeleteFile(TempName);
		DeleteFile(TempPath + BaseNameOf(TempName));
		DeleteFile(AFileName);
		ShowError('ER_UNK', IntStr(E) + FormatDelim + TempName, E, true);
	end;

begin
	{$IFOPT D+} Debug('+DECOMPRESS', AFileName + ' ('  + HexWord(AMethod) + ')'); {$ENDIF}
	TempPath := PathOf(AFileName);
	if TempPath = '' then TempPath := GetCurDir;
	TempPath := TailDelim(TempPath);
	case AMethod of
		cpNone : begin
			LastDCE := '';
			LastDCM := cpNone;
		end;
		cpGZip : begin
			if (LastDCM <> cpGZip) or (not FileExists(LastDCE)) then begin
				LastDCM := cpGZip;
				LastDCE := FindExecutable('GZIP.EXE');
				if LastDCE = '' then begin
					DeleteFile(AFileName);
	  				ShowError('ER_2', FormatDelim + 'GZip', erFile_not_found, true);
	  			end;
			end;
			repeat
				TempName := '.GZ';
				E := mkTempFile(TempPath, TempName);
				if E <> 0 then
					ShowError('ER_UNK', IntStr(E) + FormatDelim + TempName, E, true);
				DeleteFile(TempName);
				if not FileExists(TempPath + BaseNameOf(TempName)) then
					break;
			until E <> 0;
			E := FileRename(AFileName,TempName);
			if E <> 0 then DecompressFail;
			{$IFOPT D+} Debug('EXPAND FILE', TempName); {$ENDIF}
		    SwapIntVecs;
			Exec(LastDCE, '-q -d ' + TempName);
	        SwapIntVecs;
			if (not FileExists(TempPath + BaseNameOf(TempName))) then begin
			    {$IFOPT D+} Debug('EXIST', 'FALSE - Failed'); {$ENDIF}
				DecompressFail;
			end else begin
			    {$IFOPT D+} Debug('EXIST', 'TRUE - Success'); {$ENDIF}
			end;
			{ WriteLn(TempPath + BaseNameOf(TempName), '>', AFileName); }
			E := FileRename(TempPath + BaseNameOf(TempName), AFileName);
			if E <> 0 then DecompressFail;
		end;
	else
		DeleteFile(AFilename);
   		ShowError('BAD_CMP', SwitchChar + FormatDelim + 'p' +
        FormatDelim + '#' + HexWord(AMethod), erCommand_Line_Error, True);
	end;
	{$IFOPT D+} Debug('-', ''); {$ENDIF}
end;

(*
{ Set Attrib First Method; probably has issues with setting RSH attrib files }
procedure FileStamp(FileName : String; Time : LongInt; Attrib : word); assembler;
asm
	push 	ds
	mov		cx, Attrib
	{ Set Filename Pointer and Make Null Terminated }
	lds		si, FileName
	xor		ah, ah
	lodsb
	test	al, al
	jz		@@Done
	cld
	mov		bx, ax
	mov		[si+bx], ah
	mov		dx, si
	mov		ax, $4301 { set file attribute }
	int		$21
	mov		ax, 02
	jc		@@Done
	mov		ax, $3d02 { 0 read only, 1 write only, 2 read/write }
	int		$21		{ open file as read/write }
	jc		@@Done
	mov		bx, ax  { Was ok, set file handle }
	pop		ds
	push	ds
	lds     cx, Time
	push	ds
	pop		dx
	mov		ax, $5701
	int		$21		{ set file timestamp }
	jc		@@TimeError
	xor		ax, ax
@@TimeError:
	push 	ax
	mov		ah, $3e
	int		$21		{ close file }
	pop		ax
	jmp		@@Done
@@Done:
	pop		ds
	mov		DosError, ax
end;
*)

{ Reset Attrib and Set Afterwards Method; should allow timestamp of
  Readonly, System and Hidden Files }
procedure FileStamp(FileName : String; Time : LongInt; Attrib : word); assembler;
asm
	push 	ds
	{ Set Filename Pointer and Make Null Terminated }
	lds		si, FileName
	xor		ah, ah
	lodsb
	test	al, al
	jz		@@DoneAll
	cld
	mov		bx, ax
	mov		[si+bx], ah
	mov		dx, si
	mov		ax, $4301 { set file attribute }
	xor 	cx, cx
	int		$21
	mov		ax, 02
	jc		@@DoneAll
	mov		ax, $3d02 { 0 read only, 1 write only, 2 read/write }
	int		$21		{ open file as read/write }
	jc		@@DoneAll
	mov		bx, ax  { Was ok, set file handle }
	pop		ds
	push	ds
	lds     cx, Time
	push	ds
	pop		dx
	mov		ax, $5701
	int		$21		{ set file timestamp }
	jc		@@TimeError
	xor		ax, ax
@@TimeError:
	push 	ax
	mov		ah, $3e
	int		$21		{ close file }
	pop		ax
	test	ax, ax
	jnz		@@DoneAll
	pop		ds
	push	ds
	{ Set File Attrib }
	mov		cx, Attrib
	lds		dx, FileName
	inc		dx
	mov		ax, $4301 { set file attribute }
	int		$21
	mov		ax, 02
	jc		@@DoneAll
	xor		ax, ax
@@DoneAll:
	pop		ds
	mov		DosError, ax
end;