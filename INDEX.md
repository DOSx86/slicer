# File Slicer

A simple archive utility to categorize and split large files and directories across smaller medium. It requires an 8086 with EGA or better graphics. (UPX Compressed)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

· **Maintainers**: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

· **New developers**: We can extend access for you to track issues here.

· **Translators**: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SLICER.LSM

<table>
<tr><td>title</td><td>File Slicer</td></tr>
<tr><td>version</td><td>0.23 (BETA)</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-01-22</td></tr>
<tr><td>description</td><td>A simple archive and file slicing/spanning utility for DOS. (UPX Compressed)</td></tr>
<tr><td>summary</td><td>A simple archive utility to categorize and split large files and directories across smaller medium. It requires an 8086 with EGA or better graphics. (UPX Compressed)</td></tr>
<tr><td>keywords</td><td>dos 16 bit file archive span split slice</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://up.lod.bz/slicer</td></tr>
<tr><td>original&nbsp;site</td><td>http://fd.lod.bz/repos/current/pkg-html/slicer.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://gitlab.com/DOSx86/slicer</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Simplified (2-Clause) BSD License</td></tr>
</table>
